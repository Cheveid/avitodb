﻿using AvitoDB.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace AvitoDB.Abstract
{
    public interface IUserRepository
    {
        void CreateTable();
        List<UserEntity> GetUsers();
        UserEntity GetUserById(long id);
        long Insert(UserEntity user);
    }
}
