﻿using AvitoDB.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace AvitoDB.Abstract
{
    public interface ICategoryRepository
    {
        void CreateTable();
        List<CategoryEntity> GetCategories();
        CategoryEntity GetCategoryById(long id);
        long Insert(CategoryEntity category);
        void Update(CategoryEntity category);
        void Delete(long id);
    }
}
