﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AvitoDB.Abstract
{
    public interface IContextDB
    {
        ICategoryRepository CategoryRepository { get; }
        IProductRepository ProductRepository { get; }
        IUserRepository UserRepository { get; }
        void Commit();
    }
}
