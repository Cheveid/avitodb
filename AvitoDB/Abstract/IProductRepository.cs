﻿using AvitoDB.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace AvitoDB.Abstract
{
    public interface IProductRepository
    {
        void CreateTable();
        List<ProductEntity> GetProducts();
        ProductEntity GetProductById(long id);
        List<ProductEntity> GetProductBySellerId(long seller_id);
        long Insert(ProductEntity product);
    }
}
