﻿using AvitoDB.Abstract;
using AvitoDB.Concrete;
using AvitoDB.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace AvitoDB.Controllers
{
    public class WorkController
    {
        IContextDB _context;
        ICategoryRepository categoryRepository => _context.CategoryRepository;
        IProductRepository productRepository => _context.ProductRepository;
        IUserRepository userRepository => _context.UserRepository;
        ConsoleController _consoleController;

        public WorkController(IContextDB context)
        {
            _context = context;
            _consoleController = new ConsoleController();
        }

        public void Run()
        {
            bool isExit = false;
            while (!isExit)
            {
                _consoleController.PrintMenu();
                var key = Console.ReadKey();
                Console.Clear();
                
                switch (key.Key)
                {
                    case ConsoleKey.D1:
                    case ConsoleKey.NumPad1:
                        CreateTables();
                        _consoleController.WriteLine("Таблицы успешно созданы.");
                        FillTables();
                        _consoleController.WriteLine("Таблицы успешно заполнены.");
                        break;
                    case ConsoleKey.D2:
                    case ConsoleKey.NumPad2:
                        PrintCategories();
                        break;
                    case ConsoleKey.D3:
                    case ConsoleKey.NumPad3:
                        PrintProducts();
                        break;
                    case ConsoleKey.D4:
                    case ConsoleKey.NumPad4:
                        PrintUsers();
                        break;
                    case ConsoleKey.D5:
                    case ConsoleKey.NumPad5:
                        if (InsertCategory())
                        {
                            _consoleController.Write("Запись успешно добавлена в таблицу!", 2, 1);
                        }
                        else
                        {
                            _consoleController.Write("Процедура добавления категории товара прервана.", 2);
                        }
                        break;
                    case ConsoleKey.D6:
                    case ConsoleKey.NumPad6:
                        if (InsertProduct())
                        {
                            _consoleController.Write("Запись успешно добавлена в таблицу!", 2, 1);
                        }
                        else
                        {
                            _consoleController.Write("Процедура добавления товара прервана.", 2);
                        }
                        break;
                    case ConsoleKey.D7:
                    case ConsoleKey.NumPad7:
                        if (InsertUser())
                        {
                            _consoleController.Write("Запись успешно добавлена в таблицу!", 2, 1);
                        }
                        else
                        {
                            _consoleController.Write("Процедура добавления пользователя прервана.", 2);
                        }
                        break;
                    case ConsoleKey.Escape:
                    case ConsoleKey.D0:
                    case ConsoleKey.NumPad0:
                        isExit = true;
                        break;
                }

                if (!isExit)
                {
                    _consoleController.WaitAnyKey();
                }
            }
        }

        public void CreateTables()
        {
            userRepository.CreateTable();
            categoryRepository.CreateTable();
            productRepository.CreateTable();
            _context.Commit();
        }

        public void FillTables()
        {
            int minRandom = 0;
            int maxRandom = 100000;
            int cntCategories = 5;
            Random rnd = new Random();
            for (int i = 1; i <= cntCategories; i++)
            {
                CategoryEntity category = new CategoryEntity();
                category.Name = "Категория " + rnd.Next(minRandom, maxRandom);
                categoryRepository.Insert(category);
            }
            _context.Commit();

            CategoryEntity[] categories = categoryRepository.GetCategories().ToArray();

            int cntUsers = 5;
            for (int i = 1; i <= cntUsers; i++)
            {
                UserEntity user = new UserEntity();
                user.Email = "Email_" + rnd.Next(minRandom, maxRandom) + "@mail.ru";
                user.Login = "Login " + rnd.Next(minRandom, maxRandom);
                user.Password = "Password_" + rnd.Next(minRandom, maxRandom);

                int cntProducts = rnd.Next(1, 10);
                user.Products = new List<ProductEntity>();
                for (int j = 1; j <= cntProducts; j++)
                {
                    CategoryEntity category = categories[rnd.Next(0, categories.Length)];

                    ProductEntity product = new ProductEntity();
                    product.CategoryId = category.Id;
                    product.Description = "Description " + rnd.Next(minRandom, maxRandom);
                    product.Name = "Name " + rnd.Next(minRandom, maxRandom);
                    product.Price = rnd.Next(minRandom, maxRandom);
                    user.Products.Add(product);
                }

                userRepository.Insert(user);
            }
            _context.Commit();
        }

        public void PrintCategories()
        {
            _consoleController.WriteLine("Таблица \"Категории\":");
            foreach (CategoryEntity category in categoryRepository.GetCategories())
            {
                _consoleController.WriteLine(category.ToString());
            }
        }

        public void PrintProducts()
        {
            _consoleController.WriteLine("Таблица \"Товары\":");
            foreach (ProductEntity product in productRepository.GetProducts())
            {
                _consoleController.WriteLine(product.ToString());
            }
        }

        public void PrintUsers()
        {
            _consoleController.WriteLine("Таблица \"Пользователи\":");
            foreach (UserEntity user in userRepository.GetUsers())
            {
                _consoleController.WriteLine(user.ToString());
            }
        }

        public bool InsertCategory()
        {
            _consoleController.Write("Новая запись в таблицу \"Категории товаров\". Esc - выход");

            string name;
            if (!_consoleController.InpuAndCheckValue("Наименование: ", "Не введено наименование!", _consoleController.CheckValueString, out name))
            {
                return false;
            }

            CategoryEntity category = new CategoryEntity()
            {
                Name = name
            };
            _context.CategoryRepository.Insert(category);
            _context.Commit();

            return true;
        }

        public bool InsertProduct()
        {
            _consoleController.Write("Новая запись в таблицу \"Товары\". Esc - выход");

            string name;
            if (!_consoleController.InpuAndCheckValue("Наименование: ", "Не введено наименование!", _consoleController.CheckValueString, out name))
            {
                return false;
            }

            string description;
            if (!_consoleController.InpuAndCheckValue("Описание товара: ", "Не введено описание!", _consoleController.CheckValueString, out description))
            {
                return false;
            }

            decimal price;
            if (!_consoleController.InpuAndCheckValue("Стоимость товара: ", "Некорректно введена стоимость!", _consoleController.CheckValueDecimal, out price))
            {
                return false;
            }

            ConsoleController.CheckValue<long> checkCategory = (string value, out long result) =>
            {
                bool isExists = false;
                if (long.TryParse(value, out result))
                {
                    isExists = _context.CategoryRepository.GetCategoryById(result) != null;
                }
                return isExists;
            };

            long categoryId = new long();
            if (!_consoleController.InpuAndCheckValue("ID категории товара: ", "Некорректно введена категория!", checkCategory, out categoryId))
            {
                return false;
            }

            ConsoleController.CheckValue<long> checkSeller = (string value, out long result) =>
            {
                bool isExists = false;
                if (long.TryParse(value, out result))
                {
                    isExists = _context.UserRepository.GetUserById(result) != null;
                }
                return isExists;
            };

            long sellerId = new long();
            if (!_consoleController.InpuAndCheckValue("ID пользователя-продавца товара: ", "Некорректно введен пользователь!", checkSeller, out sellerId))
            {
                return false;
            }

            ProductEntity product = new ProductEntity()
            {
                CategoryId = categoryId,
                Description = description,
                Name = name,
                Price = price,
                SellerId = sellerId
            };

            _context.ProductRepository.Insert(product);
            _context.Commit();

            return true;
        }

        public bool InsertUser()
        {
            _consoleController.Write("Новая запись в таблицу \"Пользователи\". Esc - выход");

            string login;
            if (!_consoleController.InpuAndCheckValue("Логин: ", "Не введен логин!", _consoleController.CheckValueString, out login))
            {
                return false;
            }

            string password;
            if (!_consoleController.InpuAndCheckValue("Пароль: ", "Не введен пароль!", _consoleController.CheckValueString, out password))
            {
                return false;
            }

            string email;
            if (!_consoleController.InpuAndCheckValue("Email: ", "Не введен email!", _consoleController.CheckValueString, out email))
            {
                return false;
            }

            UserEntity user = new UserEntity()
            {
                Email = email,
                Login = login,
                Password = password
            };

            _context.UserRepository.Insert(user);
            _context.Commit();

            return true;
        }
    }
}
