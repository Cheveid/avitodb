﻿using AvitoDB.Abstract;
using System;
using System.Collections.Generic;
using System.Text;

namespace AvitoDB.Controllers
{
    public class ConsoleController
    {
        public delegate bool CheckValue<T>(string value, out T result);

        public void PrintMenu()
        {
            Console.Clear();
            WriteLine("Меню:");
            WriteLine("1. Создать таблицы и заполнить данными");
            WriteLine("2. Вывести таблицу \"Категории товаров\"");
            WriteLine("3. Вывести таблицу \"Товары\"");
            WriteLine("4. Вывести таблицу \"Пользователи\"");
            WriteLine("5. Добавить запись в таблицу \"Категории товаров\"");
            WriteLine("6. Добавить запись в таблицу \"Товары\"");
            WriteLine("7. Добавить запись в таблицу \"Пользователи\"");
            WriteLine("0. Выход (Esc)");
            WriteLine();
            Write("Ваш выбор: ");
        }

        public string readLineWithCancel()
        {
            string result = null;

            StringBuilder buffer = new StringBuilder();

            ConsoleKeyInfo info = Console.ReadKey(true);
            while (info.Key != ConsoleKey.Enter && info.Key != ConsoleKey.Escape)
            {
                if (info.Key == ConsoleKey.Backspace)
                {
                    if (buffer.Length > 0)
                    {
                        buffer = buffer.Remove(buffer.Length - 1, 1);
                        Console.CursorLeft = Console.CursorLeft - 1;
                        Write(" ");
                        Console.CursorLeft = Console.CursorLeft - 1;
                    }
                }
                else
                {
                    Write(info.KeyChar.ToString());
                    buffer.Append(info.KeyChar);
                }
                info = Console.ReadKey(true);
            }

            if (info.Key == ConsoleKey.Enter)
            {
                result = buffer.ToString();
            }

            return result;
        }

        public void WaitAnyKey()
        {
            WriteLine();
            WriteLine($"Для продолжения нажмите любую клавишу...");
            Console.ReadKey();
        }

        public void Write(string text = null, ConsoleColor? color = null)
        {
            if (color != null)
            {
                ConsoleColor colorPrev = Console.ForegroundColor;
                Console.ForegroundColor = color.Value;
                Console.Write(text);
                Console.ForegroundColor = colorPrev;
            }
            else
            {
                Console.Write(text);
            }
        }

        public void Write(string text, int linesBefore, int linesAfter = 0, ConsoleColor? color = null)
        {
            StringBuilder sbText = new StringBuilder();

            for (int i = 0; i < linesBefore; i++)
            {
                sbText.Append($"{Environment.NewLine}");
            }

            sbText.Append(text);

            for (int i = 0; i < linesAfter; i++)
            {
                sbText.Append($"{Environment.NewLine}");
            }

            Write(sbText.ToString(), color);
        }

        public void WriteLine(string text = null, ConsoleColor? color = null)
        {
            Write(text, 0, 1, color);
        }

        public CheckValue<string> CheckValueString = (string value, out string result) =>
        {
            result = string.Empty;
            if (!string.IsNullOrEmpty(value))
            {
                result = value;
            }
            return result != string.Empty;
        };

        public CheckValue<decimal> CheckValueDecimal = (string value, out decimal result) =>
        {
            result = new decimal();
            return decimal.TryParse(value, out result);
        };

        public bool InpuAndCheckValue<T>(string message, string messageError, CheckValue<T> checkValue, out T result)
        {
            bool isCorrect = false;
            string value = "";
            result = default(T);
            WriteLine();
            while (!isCorrect)
            {
                Write(message);
                value = readLineWithCancel();

                if (value == null)
                    return false;

                if (checkValue(value, out result))
                {
                    isCorrect = true;
                }
                else
                {
                    WriteLine(messageError, ConsoleColor.Red);
                }
            }
            return true;
        }
    }
}
