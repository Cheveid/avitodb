﻿using AvitoDB.Abstract;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Text;

namespace AvitoDB.Concrete
{
    public class ContextDB : IContextDB
    {
        string _connectionString;
        NpgsqlConnection _connection;
        NpgsqlTransaction _transaction;
        ICategoryRepository _categoryRepository;
        IProductRepository _productRepository;
        IUserRepository _userRepository;

        public ContextDB(string connectionString)
        {
            _connectionString = connectionString;
            _connection = new NpgsqlConnection(_connectionString);
            _connection.Open();
            _transaction = _connection.BeginTransaction();
        }

        public ICategoryRepository CategoryRepository => _categoryRepository ??= new CategoryRepository(_transaction);

        public IProductRepository ProductRepository => _productRepository ??= new ProductRepository(_transaction);

        public IUserRepository UserRepository => _userRepository ??= new UserRepository(_transaction, ProductRepository);

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public void Commit()
        {
            try
            {
                _transaction.Commit();
            }
            catch (Exception ex)
            {
                _transaction.Rollback();
                throw new Exception("Ошибка при завершении транзакции", ex);
            }
            finally
            {
                _transaction.Dispose();
                _transaction = _connection.BeginTransaction();
                _categoryRepository = null;
                _productRepository = null;
                _userRepository = null;
            }
        }
    }
}
