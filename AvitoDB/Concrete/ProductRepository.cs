﻿using AvitoDB.Abstract;
using AvitoDB.Entities;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Text;

namespace AvitoDB.Concrete
{
    public class ProductRepository : IProductRepository
    {
        NpgsqlTransaction _transaction;
        NpgsqlConnection _connection => _transaction?.Connection;

        const string F_ID = "id";
        const string F_NAME = "name";
        const string F_DESCRIPTION = "description";
        const string F_PRICE = "price";
        const string F_CATEGORY_ID = "category_id";
        const string F_DATE_CREATE = "date_create";
        const string F_SELLER_ID = "seller_id";
        const string F_IS_ACTIVE = "is_active";

        public ProductRepository(NpgsqlTransaction transaction)
        {
            _transaction = transaction;
        }

        public void CreateTable()
        {
            var sql = @$"
                CREATE SEQUENCE if not exists products_id_seq;

                CREATE TABLE if not exists products
                (
                    {F_ID}          BIGINT                  NOT NULL    DEFAULT NEXTVAL('products_id_seq'),
                    {F_NAME}        CHARACTER VARYING(255)  NOT NULL,
                    {F_DESCRIPTION} CHARACTER VARYING(255)  NOT NULL,
                    {F_PRICE}       NUMERIC (10, 2)         NOT NULL,
                    {F_CATEGORY_ID} BIGINT                  NOT NULL,
                    {F_DATE_CREATE} TIMESTAMP               NOT NULL    DEFAULT CURRENT_TIMESTAMP,
                    {F_SELLER_ID}   BIGINT                  NOT NULL,
                    {F_IS_ACTIVE}   BOOLEAN                 NOT NULL    DEFAULT TRUE,
                    
                    CONSTRAINT products_pkey PRIMARY KEY ({F_ID}),
                    CONSTRAINT products_fk_category_id FOREIGN KEY ({F_CATEGORY_ID}) REFERENCES categories (id),
                    CONSTRAINT products_fk_seller_id FOREIGN KEY ({F_SELLER_ID}) REFERENCES users (id) ON DELETE CASCADE
                );

                CREATE INDEX if not exists products_name_idx ON products({F_NAME});
                CREATE INDEX if not exists products_category_idx ON products({F_CATEGORY_ID});
                CREATE INDEX if not exists products_seller_idx ON products({F_SELLER_ID});
            ";

            using var command = new NpgsqlCommand(sql, _connection);
            command.ExecuteNonQuery().ToString();
        }

        public List<ProductEntity> GetProducts()
        {
            var sql = SELECT_PRODUCT;

            using var command = new NpgsqlCommand(sql, _connection);
            NpgsqlDataReader reader = command.ExecuteReader();

            List<ProductEntity> listProducts = ReaderToList(reader);

            return listProducts;
        }

        public ProductEntity GetProductById(long id)
        {
            var sql = SELECT_PRODUCT + @$"
                where p.{F_ID} = @id;
            ";

            using var command = new NpgsqlCommand(sql, _connection);
            var parameter = command.Parameters;
            parameter.Add(new NpgsqlParameter("id", id));

            NpgsqlDataReader reader = command.ExecuteReader();

            List<ProductEntity> listProducts = ReaderToList(reader);

            return listProducts.Count > 0 ? listProducts[0] : null;
        }

        public List<ProductEntity> GetProductBySellerId(long seller_id)
        {
            var sql = SELECT_PRODUCT + @$"
                where p.{F_SELLER_ID} = @seller_id;
            ";

            using var command = new NpgsqlCommand(sql, _connection);
            var parameter = command.Parameters;
            parameter.Add(new NpgsqlParameter("seller_id", seller_id));

            NpgsqlDataReader reader = command.ExecuteReader();

            List<ProductEntity> listProducts = ReaderToList(reader);

            return listProducts;
        }

        public long Insert(ProductEntity product)
        {
            if (product == null)
            {
                throw new ArgumentNullException();
            }

            var sql = @$"
                insert into products ({F_NAME}, {F_DESCRIPTION}, {F_PRICE}, {F_SELLER_ID}, {F_IS_ACTIVE}, {F_CATEGORY_ID})
                values (@name, @description, @price, @seller_id, @is_active, @category_id)
                returning {F_ID};
            ";

            using var command = new NpgsqlCommand(sql, _connection, _transaction);
            var parameters = command.Parameters;
            parameters.Add(new NpgsqlParameter("name", product.Name));
            parameters.Add(new NpgsqlParameter("description", product.Description));
            parameters.Add(new NpgsqlParameter("price", product.Price));
            parameters.Add(new NpgsqlParameter("seller_id", product.SellerId));
            parameters.Add(new NpgsqlParameter("is_active", product.IsActive));
            parameters.Add(new NpgsqlParameter("category_id", product.CategoryId));
            var productId = (long)command.ExecuteScalar();

            return productId;
        }


        string SELECT_PRODUCT = @$"
            select  p.{F_ID}, 
                    p.{F_NAME},
                    p.{F_DESCRIPTION},
                    p.{F_PRICE},
                    p.{F_CATEGORY_ID},
                    p.{F_DATE_CREATE},
                    p.{F_SELLER_ID},
                    p.{F_IS_ACTIVE},
                    c.name category_name
            from products p
            join categories c on p.{F_CATEGORY_ID} = c.id
        ";

        List<ProductEntity> ReaderToList(NpgsqlDataReader reader)
        {
            List<ProductEntity> listProducts = new List<ProductEntity>();

            while (reader.Read())
            {
                listProducts.Add(new ProductEntity()
                {
                    Category = new CategoryEntity()
                    {
                        Id = (long)reader[F_CATEGORY_ID],
                        Name = reader["category_name"].ToString()
                    },
                    CategoryId = (long)reader[F_CATEGORY_ID],
                    DateCreate = (DateTime)reader[F_DATE_CREATE],
                    Description = reader[F_DESCRIPTION].ToString(),
                    Id = (long)reader[F_ID],
                    IsActive = (bool)reader[F_IS_ACTIVE],
                    Name = reader[F_NAME].ToString(),
                    Price = (decimal)reader[F_PRICE],
                    SellerId = (long)reader[F_SELLER_ID]
                });
            }

            reader.Close();

            return listProducts;
        }
    }
}
