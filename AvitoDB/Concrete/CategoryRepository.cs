﻿using AvitoDB.Abstract;
using AvitoDB.Entities;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Text;

namespace AvitoDB.Concrete
{
    public class CategoryRepository : ICategoryRepository
    {
        NpgsqlTransaction _transaction;
        NpgsqlConnection _connection => _transaction?.Connection;

        const string F_ID = "id";
        const string F_NAME = "name";

        public CategoryRepository(NpgsqlTransaction transaction)
        {
            _transaction = transaction;
        }

        public void CreateTable()
        {
            var sql = @$"
                CREATE SEQUENCE if not exists categories_id_seq;

                CREATE TABLE if not exists categories
                (
                    {F_ID}      BIGINT                  NOT NULL    DEFAULT NEXTVAL('categories_id_seq'),
                    {F_NAME}    CHARACTER VARYING(255)  NOT NULL,
  
                    CONSTRAINT categories_pkey PRIMARY KEY ({F_ID})
                );

                CREATE INDEX if not exists categories_name_idx ON categories({F_NAME});
            ";

            using var command = new NpgsqlCommand(sql, _connection);
            command.ExecuteNonQuery().ToString();
        }

        public List<CategoryEntity> GetCategories()
        {
            var sql = @"
                select * from categories;
            ";

            List<CategoryEntity> listCategories = new List<CategoryEntity>();

            using var command = new NpgsqlCommand(sql, _connection);
            NpgsqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                listCategories.Add(new CategoryEntity()
                {
                    Id = (long)reader[F_ID],
                    Name = reader[F_NAME].ToString()
                });
            }
            reader.Close();

            return listCategories;
        }

        public CategoryEntity GetCategoryById(long id)
        {
            var sql = @$"
                select * from categories where {F_ID} = @id;
            ";

            List<CategoryEntity> listCategories = new List<CategoryEntity>();

            using var command = new NpgsqlCommand(sql, _connection);
            var parameters = command.Parameters;
            parameters.Add(new NpgsqlParameter("id", id));

            CategoryEntity category = null;

            NpgsqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                reader.Read();

                category = new CategoryEntity()
                {
                    Id = (long)reader[F_ID],
                    Name = reader[F_NAME].ToString()
                };
            }
            reader.Close();

            return category;
        }

        public long Insert(CategoryEntity category)
        {
            if (category == null)
            {
                throw new ArgumentNullException();
            }

            var sql = @$"
                insert into categories ({F_NAME})
                values (@name)
                returning {F_ID};
            ";

            using var command = new NpgsqlCommand(sql, _connection, _transaction);
            var parameters = command.Parameters;
            parameters.Add(new NpgsqlParameter("name", category.Name));
            var categoryId = (long)command.ExecuteScalar();

            return categoryId;
        }

        public void Update(CategoryEntity category)
        {
            if (category == null)
            {
                throw new ArgumentNullException();
            }

            var sql = @$"
                update categories set {F_NAME} = @name where {F_ID} = @id
            ";

            using var command = new NpgsqlCommand(sql, _connection, _transaction);
            var parameters = command.Parameters;
            parameters.Add(new NpgsqlParameter("id", category.Id));
            parameters.Add(new NpgsqlParameter("name", category.Name));
            command.ExecuteNonQuery();
        }

        public void Delete(long id)
        {
            var sql = @"
                select count(*) cnt from products where category_id = @category_id
            ";
            
            using var command_count = new NpgsqlCommand(sql, _connection);
            var parameters_count = command_count.Parameters;
            parameters_count.Add(new NpgsqlParameter("category_id", id));
            long cnt = (long)command_count.ExecuteScalar();
            if (cnt > 0)
            {
                throw new NpgsqlException("Удаление категории невозможно, т.к. существуют товары с данной категорией!");
            }

            sql = @$"
                delete from categories where {F_ID} = @id
            ";

            using var command = new NpgsqlCommand(sql, _connection, _transaction);
            var parameters = command.Parameters;
            parameters.Add(new NpgsqlParameter("id", id));
            command.ExecuteNonQuery();
        }
    }
}
