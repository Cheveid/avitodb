﻿using AvitoDB.Abstract;
using AvitoDB.Entities;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Text;

namespace AvitoDB.Concrete
{
    public class UserRepository : IUserRepository
    {
        NpgsqlTransaction _transaction;
        NpgsqlConnection _connection => _transaction?.Connection;
        IProductRepository _productRepository;

        const string F_ID = "id";
        const string F_LOGIN = "login";
        const string F_PASSWORD = "password";
        const string F_EMAIL = "email";
        const string F_DATE_CREATE = "date_create";

        public UserRepository(NpgsqlTransaction transaction, IProductRepository productRepository)
        {
            _transaction = transaction;
            _productRepository = productRepository;
        }

        public void CreateTable()
        {
            var sql = @$"
                CREATE SEQUENCE if not exists users_id_seq;

                CREATE TABLE if not exists users
                (
                    {F_ID}          BIGINT                  NOT NULL    DEFAULT NEXTVAL('users_id_seq'),
                    {F_LOGIN}       CHARACTER VARYING(255)  NOT NULL,
                    {F_PASSWORD}    CHARACTER VARYING(255)  NOT NULL,
                    {F_EMAIL}       CHARACTER VARYING(255)  NOT NULL,
                    {F_DATE_CREATE} TIMESTAMP               NOT NULL    DEFAULT CURRENT_TIMESTAMP,
                    
                    CONSTRAINT users_pkey PRIMARY KEY ({F_ID})
                );

                CREATE INDEX if not exists users_login_idx ON users({F_LOGIN});
            ";

            using var command = new NpgsqlCommand(sql, _connection);
            command.ExecuteNonQuery().ToString();
        }

        public List<UserEntity> GetUsers()
        {
            var sql = SELECT_USER;

            using var command = new NpgsqlCommand(sql, _connection);
            NpgsqlDataReader reader = command.ExecuteReader();

            List<UserEntity> listUsers = ReaderToList(reader, _productRepository);

            return listUsers;
        }

        public UserEntity GetUserById(long id)
        {
            var sql = SELECT_USER + @$"
                where {F_ID} = @id
            ";

            using var command = new NpgsqlCommand(sql, _connection);
            var parameters = command.Parameters;
            parameters.Add(new NpgsqlParameter("id", id));

            NpgsqlDataReader reader = command.ExecuteReader();

            List<UserEntity> listUsers = ReaderToList(reader, _productRepository);

            return listUsers?.Count > 0 ? listUsers[0] : null;
        }

        public long Insert(UserEntity user)
        {
            if (user == null)
            {
                throw new ArgumentNullException();
            }

            var sql = @$"
                    insert into users ({F_LOGIN}, {F_PASSWORD}, {F_EMAIL})
                    values (@login, @password, @email)
                    returning {F_ID};
                ";

            using var command = new NpgsqlCommand(sql, _connection, _transaction);
            var parameters = command.Parameters;
            parameters.Add(new NpgsqlParameter("login", user.Login));
            parameters.Add(new NpgsqlParameter("password", user.Password));
            parameters.Add(new NpgsqlParameter("email", user.Email));
            var userId = (long)command.ExecuteScalar();

            if (user.Products != null)
            {
                foreach (ProductEntity product in user.Products)
                {
                    product.SellerId = userId;
                    _productRepository.Insert(product);
                }
            }

            return userId;
        }


        string SELECT_USER = @$"
            select  {F_ID}, 
                    {F_LOGIN},
                    {F_PASSWORD},
                    {F_EMAIL},
                    {F_DATE_CREATE}
            from users
        ";

        List<UserEntity> ReaderToList(NpgsqlDataReader reader, IProductRepository productRepository)
        {
            List<UserEntity> listUsers = new List<UserEntity>();

            while (reader.Read())
            {
                listUsers.Add(new UserEntity()
                {
                    Email = reader[F_EMAIL].ToString(),
                    Id = (long)reader[F_ID],
                    Login = reader[F_LOGIN].ToString(),
                    Password = reader[F_PASSWORD].ToString()
                });
            }

            reader.Close();

            foreach (UserEntity user in listUsers)
            {
                user.Products = productRepository?.GetProductBySellerId(user.Id);
            }

            return listUsers;
        }
    }
}
