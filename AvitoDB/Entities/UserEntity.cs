﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AvitoDB.Entities
{
    public class UserEntity
    {
        public long Id { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public IList<ProductEntity> Products { get; set; }

        public override string ToString()
        {
            return $"{Id}. {Login}. {Password}. {Email}";
        }
    }
}
