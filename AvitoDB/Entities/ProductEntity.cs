﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AvitoDB.Entities
{
    public class ProductEntity
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public long SellerId { get; set; }
        public DateTime DateCreate { get; set; }
        public bool IsActive { get; set; } = true;
        public long CategoryId { get; set; }
        public CategoryEntity Category { get; set; }

        public override string ToString()
        {
            return $"{Id}. {Name}. {Description}. {Price}. {SellerId}. {DateCreate}. {IsActive}. {CategoryId}";
        }
    }
}
