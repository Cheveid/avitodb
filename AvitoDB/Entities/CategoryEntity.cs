﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AvitoDB.Entities
{
    public class CategoryEntity
    {
        public long Id { get; set; }
        public string Name { get; set; }

        public override string ToString()
        {
            return $"{Id}. {Name}";
        }
    }
}
