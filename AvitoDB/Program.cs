﻿using AvitoDB.Abstract;
using AvitoDB.Concrete;
using AvitoDB.Controllers;
using AvitoDB.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace AvitoDB
{
    class Program
    {
        const string connectionString = "Host=localhost;Username=AvitoUser;Password=AvitoUser;Database=AvitoDB";

        static void Main(string[] args)
        {
            IContextDB contextDB = new ContextDB(connectionString);

            WorkController workController = new WorkController(contextDB);
            workController.Run();
        }
    }
}
